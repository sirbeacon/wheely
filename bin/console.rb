#!/usr/bin/env ruby

require_relative '../system/wheely/container'

Wheely::Container.finalize!

require 'irb'
IRB.start
