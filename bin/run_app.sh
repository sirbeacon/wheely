#!/bin/bash

rm -f /tmp/unicorn.pid

APP_ENV=production bundle exec unicorn -E production -c config/unicorn.rb
