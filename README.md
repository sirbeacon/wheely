# Wheely app

## Basic installation

Create gemset and install gems:

```
  rvm ruby-2.7.1 do rvm gemset create wheely
  rvm use ruby-2.7.1@wheely

  gem install bundler

  bundle install
```

Run application using docker:

Docker-compose should build all the necessary images
```
  docker-compose up
```

Run application:

```
  bundle exec thin start -p 3000
```

Run workaround which is alternative to `rails c` command:

```
  bundle exec bin/console.rb
```

Run tests
```
bundle exec rpsec spec/
```

## Basic usage

```
  curl 'localhost:3000/api/cars?lat=55.752992&lng=37.618333
  # 1
```
