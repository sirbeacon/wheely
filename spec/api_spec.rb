require_relative '../system/wheely/container'

require 'dry/system/stubs'
require 'dry/monads/result'
require 'rack/test'

Wheely::Container.enable_stubs!

begin
  require 'pry-byebug'
  rescue LoadError
end

describe 'Api tests' do
  include Dry::Monads::Result::Mixin
  include Rack::Test::Methods

  before(:each) do
    Wheely::Container['memcache'].flush
  end

  def app
    Wheely::Container['api.car']
  end

  describe 'checks car method' do
    let(:return_get_value)  {[{:id=>166, :lat=>55.7575429, :lng=>37.6135117},
                              {:id=>229, :lat=>55.74837156167371, :lng=>37.61180107665421},
                              {:id=>8, :lat=>55.7532706, :lng=>37.6076902}]}
    let(:return_post_value) {[1,1,1]}
    let(:status_code)       { '200' }

    before(:each) do
      mock = double('Request handler mock')
      allow(mock).to receive(:make_get_request).with(any_args)
                         .and_return({value: return_get_value,
                                      code: status_code,
                                      uri: Wheely::Container['app.config']['app']['cars']})
      allow(mock).to receive(:make_post_request).with(any_args)
                         .and_return({value: return_post_value,
                                      code: status_code,
                                      uri: Wheely::Container['app.config']['app']['predict']})
      Wheely::Container.stub('gateways.request_handler', mock)
    end

      it 'gets success' do
        get '/cars', lat: 55.752992, lng: 37.618333 do
          expect(last_response.body).to eq('1')
      end
    end
  end

  describe 'Gets wrong status code' do
    let(:status_code) { '500' }

    before(:each) do
      mock = double('Request handler mock')
      allow(mock).to receive(:make_get_request).with(any_args)
                         .and_return({code: status_code,
                                      uri: Wheely::Container['app.config']['app']['cars']})
      Wheely::Container.stub('gateways.request_handler', mock)
    end

    it 'gets 500 error in timeout' do
      get '/cars', lat: 55.752992, lng: 37.618333 do
        wrong_status_code = 'Got wrong error 500 code from https://dev-api.wheely.com/fake-eta/cars'
        expect(last_response.status).to eq(500)
        expect(last_response.body).to eq({wrong_status_code: wrong_status_code}.to_json)
      end
    end
  end

  describe 'Checks params' do
    it 'warns about wrong params' do
      get '/cars', lng: 999_999, lat: 'lat'
      expect(last_response.status).to eq(400)
      expect(JSON.parse(last_response.body)).to eq({ 'lat' => ['must be a float'],
                                                     'lng' => ['must be less than 180.0'] })
    end
  end
end
