root = File.join(File.dirname(__FILE__), '../')

working_directory root
worker_processes((ENV['UNICORN_WORKERS'] || 8).to_i)
timeout((ENV['UNICORN_TIMEOUT'] || 120).to_i)

listen '0.0.0.0:3000', backlog: 2048
pid '/tmp/unicorn.pid'

preload_app true
