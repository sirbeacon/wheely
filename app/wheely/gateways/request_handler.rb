require 'net/http'

module Wheely
  module Gateways
    class RequestHandler
      def make_post_request(uri, params)
        uri = URI(uri)
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = uri.scheme == 'https'

        request = Net::HTTP::Post.new(uri.request_uri)
        request.body = JSON.dump(params)

        request['Content-Type'] = 'application/json'
        response = http.request(request)

        log_response(response, uri)

        {value: JSON.parse(response.body, symbolize_names: true), uri: uri, code: response.code}
      end

      def make_get_request(uri, params)
        uri = URI(uri)
        uri.query = URI.encode_www_form(params)
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = uri.scheme == 'https'

        request = Net::HTTP::Get.new(uri.request_uri)
        response = http.request(request)

        log_response(response, uri)

        {value: JSON.parse(response.body, symbolize_names: true), uri: uri, code: response.code}
      end

      def log_response(response, uri)
        Wheely::Container['logger'].info("Got response from #{uri}, code #{response.code}, body: #{response.body}")
      end
    end
  end
end
