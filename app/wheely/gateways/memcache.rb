module Wheely
  module Gateways
    class Memcache
      def cache(key, &block)
        output = Wheely::Container['memcache'].get(key)

        if output.nil?
          output = block.()
          Wheely::Container['memcache'].set(key, output,
                                            Wheely::Container['app.config']['memcached']['expires_in'])
        end
        output
      end
    end
  end
end
