require 'dry/monads/result'

module Wheely
  module Gateways
    class RequestHandlerWrapper
      include Dry::Monads::Result::Mixin
      include Wheely::Import['gateways.request_handler']

      def make_get_request(uri, params)
        wrap_response(request_handler.make_get_request(uri, params))
      end

      def make_post_request(uri, params)
        wrap_response(request_handler.make_post_request(uri, params))
      end

      def wrap_response(res)
        if res[:code] == '200'
          Success(res[:value])
        else
          Failure([500, wrong_status_code: "Got wrong error #{res[:code]} code from #{res[:uri]}"])
        end
      end
    end
  end
end
