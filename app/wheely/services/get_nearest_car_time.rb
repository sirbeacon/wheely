require 'dry/schema'
require 'dry/monads/result'

module Wheely
  module Services
    class GetNearestCarTime
      include Wheely::Import['gateways.request_handler_wrapper',
                             'app.config',
                             'gateways.memcache']

      def call(params)
        memcache.cache "#{params[:lng]},#{params[:lat]}" do
          request_handler_wrapper.make_get_request(config['app']['cars'],
                                                   params.merge(limit: config['app']['limit']))
              .fmap { |cars| { target: params, source: cars.map { |el| {lng: el[:lng], lat: el[:lat] } } } }
              .bind { |predict_params| request_handler_wrapper.make_post_request(config['app']['predict'], predict_params) }
              .fmap { |el| el.min }
        end
      end
    end
  end
end
