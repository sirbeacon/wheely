require 'dry/schema'
require 'dry/monads/result'

module Wheely
  module Services
    class CheckParams
      include Dry::Monads::Result::Mixin

      Schema = Dry::Schema.Params do
        required(:lng).filled(:float, gt?: -180.0, lt?: 180.0)
        required(:lat).filled(:float, gt?: -90.0, lt?: 90.0)
      end

      def call(params)
        result = Schema.(params)

        if result.success?
          Success(result.to_h)
        else
          Failure([400, result.errors.to_h])
        end
      end
    end
  end
end
