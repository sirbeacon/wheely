require 'sinatra/base'
require 'sinatra/json'

module Wheely
  module Api
    class Car < Sinatra::Application
      include Wheely::Import['services.check_params',
                             'services.get_nearest_car_time']

      get '/cars' do
        render_from_result(check_params.(params)
                               .bind { |el| get_nearest_car_time.(el) })
      end

      private

      def render_from_result(result)
        if result.success?
          status 200
          json result.value!
        else
          status result.failure[0]
          json result.failure[1]
        end
      end
    end
  end
end
