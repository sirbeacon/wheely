FROM ruby:2.7.1

RUN mkdir -p /opt/app/wheely

RUN useradd --uid 2016 --home /opt/app/wheely --shell /bin/bash --comment "wheely" wheely

RUN apt-get update && apt-get install --no-install-recommends -y \
  wget \
  gnupg \
  build-essential \
  git \
  less \
  libpq-dev \
  libxml2-dev \
  libxml2 \
  libxslt-dev \
  make \
  postgresql-client \
  pkg-config \
  ruby-dev

RUN chown -R wheely /opt/app/wheely

USER wheely
WORKDIR /opt/app/wheely

RUN gem install bundler
COPY --chown=wheely:wheely Gemfile Gemfile.lock config.ru /opt/app/wheely/
RUN bundle install

COPY --chown=wheely:wheely app /opt/app/wheely/app
COPY --chown=wheely:wheely bin /opt/app/wheely/bin
COPY --chown=wheely:wheely config /opt/app/wheely/config
COPY --chown=wheely:wheely system /opt/app/wheely/system

WORKDIR /opt/app/wheely

EXPOSE 3000

ENTRYPOINT ["/opt/app/wheely/bin/run_app.sh"]
