require 'dry/system/container'
require 'dry/system/components'
require 'yaml'
require 'dalli'

module Wheely
  class Container < Dry::System::Container
    use :logging
    use :env, inferrer: -> { ENV.fetch('RACK_ENV', :development).to_sym }

    configure do |config|
      config.default_namespace = 'wheely'
      config.logger = Logger.new($stdout)
      config.auto_register = %w[app]
    end

    register('app.config', YAML.load_file('config/config.yml'))

    load_paths!('app', 'system')

    register(:memcache) do
      Dalli::Client.new(Wheely::Container['app.config']['memcached']['host'],
                        expires_in: Wheely::Container['app.config']['memcached']['expires_in'],
                        compress: true)
    end
  end

  Import = Container.injector
end
